import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ProductTypeService } from '../providers/product-type-service';
import { BrandService } from '../providers/brand-service';
import { ModelService } from '../providers/model-service';
import { AuthService } from '../providers/auth-service';

declare var $:any;
declare var swal:any;

@Component({
    moduleId: module.id,
    selector: 'model',
    templateUrl: 'model.component.html',
    styleUrls: ['model.component.scss']
})

export class ModelComponent implements OnInit{
    status_id: any = "1";
    product_type_list: any;
    brand_list: any;
    total: any;
    page: any = 1;
    limit: any = 10;
    search_name: any = "";
    model_list: any;
    model:any = {
        "name": "",
        "product_type_id" : "",
        "brand_id" : ""
    };
    
    constructor (public productTypeService: ProductTypeService, public brandService:BrandService, public modelService:ModelService, public auth:AuthService){
    }

    ngOnInit(){
        if(this.auth.checkLogin()){
            this.loadBrand();
            this.loadModel(this.page, this.limit, this.status_id, "");
            this.loadProductType();
        }
    }

    ngAfterViewInit(){
        // Init Tooltips
        $('[rel="tooltip"]').tooltip();
    }

    ngOnDestroy(){
        $("body>#addModal").remove();
        $("body>#editModal").remove();
    }

    onSearch(data){
        this.search_name = data
        this.page = 1
        this.loadModel(this.page, this.limit, this.status_id, this.search_name)
    }

    loadBrand(){
        this.brandService.getBrandList(1,100,1,"").then((response:any)=>{
            this.brand_list = response['data']
        },error=>{
        })
    }

    loadProductType(){
        this.productTypeService.getProductTypeList(1,100,1,"").then((response:any)=>{
            this.product_type_list = response['data']
        },error=>{
            this.failAlert(error.message)
        })
    }

    loadModel(page, limit, status_id, search_by){
        this.modelService.getModelList(page, limit, status_id, search_by).then((response:any)=>{
            if(response['data'].length == 0){
                this.failAlert("No Result")
            }else{
                this.model_list = response['data']
                this.total = response['total']
            }
        },error=>{
            this.failAlert(error.message)
        })
    }

    successAlert(message) {
        swal({
            type: 'success',
            title: 'Successful',
            text: message,
            confirmButtonClass: 'btn btn-success',
            buttonsStyling: false
        })
    }

    failAlert(message) {
        swal({
            title: 'Failed',
            text: message,
            type: 'error',
            confirmButtonClass: "btn btn-info",
            buttonsStyling: false
        })
    }

    loadingAlert() {
        swal({
            title: 'Loading...',
            text: ' ',
            onOpen: () => {
              swal.showLoading()
            }
        })
    }

    clearInput(){
        this.model = {
            "name": "",
            "product_type_id" : "",
            "brand_id" : ""
        }
    }

    addModal() {
        this.clearInput();
        $("#addModal").modal("show");
        $("#addModal").appendTo("body");
    }

    editModal(data) {
        this.model = data;
        $("#editModal").modal("show");
        $("#editModal").appendTo("body");
    }

    addModel() {
        if((this.model['name'] == "") || (this.model['name'].length < 2)){
            this.failAlert("Name are required and minimum 2 characters.")
        }else if(this.model['brand_id'] == null){
            this.failAlert("Please choose one brand.")
        }else if(this.model['product_type_id'] == null){
            this.failAlert("Please choose one product type.")
        }else {
            this.loadingAlert()
            this.modelService.addModel(this.model).then((response:any)=>{
                $("#addModal").modal("hide");
                this.loadModel(this.page, this.limit, this.status_id, this.search_name);
                this.successAlert(response.message)
            },error=>{
                this.failAlert(error.message)
            })
        }
    }

     editModel() {
        if((this.model['name'] == "") || (this.model['name'].length < 2)){
            this.failAlert("Name are required and minimum 2 characters.")
        }else if(this.model['brand_id'] == null){
            this.failAlert("Please choose one brand.")
        }else if(this.model['product_type_id'] == null){
            this.failAlert("Please choose one product type.")
        }else {
            this.loadingAlert()
            this.modelService.updateModel(this.model['id'], this.model).then((response:any)=>{
                $("#editModal").modal("hide");
                this.loadModel(this.page, this.limit, this.status_id, this.search_name);
                this.successAlert(response.message)
            },error=>{
                this.failAlert(error.message)
            })
        }
    }

    deleteModel(model_id) {
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            confirmButtonText: 'Yes, delete it!',
            buttonsStyling: false
        }).then((confirm)=> {
            if(confirm) {
                this.modelService.deleteModel(model_id).then((response:any)=>{
                    this.loadModel(this.page, this.limit, this.status_id, this.search_name);
                    this.successAlert(response.message)
                },error=>{
                    this.failAlert(error.message)
                })
            }
        }).catch(swal.noop)
    }

    goToPage(n: number): void {
        this.page = n;
        this.loadModel(this.page, this.limit, this.status_id, this.search_name);
    }

    onNext(): void {
        this.page++;
        this.loadModel(this.page, this.limit, this.status_id, this.search_name);
    }

    onPrev(): void {
        this.page--;
        this.loadModel(this.page, this.limit, this.status_id, this.search_name);
    }
}
