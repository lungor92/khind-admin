import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { WarrantyDetailComponent } from './warranty-detail.component';
import { WarrantyDetailRoutes } from './warranty-detail.routing';
import { PaginationModule } from '../components/pagination/pagination.module';
@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(WarrantyDetailRoutes),
        FormsModule,
        ReactiveFormsModule,
        PaginationModule
    ],
    declarations: [WarrantyDetailComponent]
})

export class WarrantyDetailModule {}
