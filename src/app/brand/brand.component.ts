import { Component, OnInit, AfterViewInit } from '@angular/core';
import { BrandService } from '../providers/brand-service';
import { AuthService } from '../providers/auth-service';

declare var $:any;
declare var swal:any;

@Component({
    moduleId: module.id,
    selector: 'brand',
    templateUrl: 'brand.component.html',
    styleUrls: ['brand.component.scss']
})

export class BrandComponent implements OnInit{

    status_id: any = "1";
    brand_list: any;
    total: any;
    page: any = 1;
    limit: any = 10;
    search_name: any = "";
    brand: any = {
        "name" : ""
    };

    constructor (public brandService: BrandService, public auth:AuthService){
    }

    ngOnInit(){
        if(this.auth.checkLogin()){
            this.loadBrand(this.page, this.limit, this.status_id, "");
        }
    }

    ngAfterViewInit(){
        // Init Tooltips
        $('[rel="tooltip"]').tooltip();
    }

    ngOnDestroy(){
        $("body>#addModal").remove();
        $("body>#editModal").remove();
    }

    onSearch(data){
        this.search_name = data
        this.page = 1
        this.loadBrand(this.page, this.limit, this.status_id, this.search_name)
    }

    loadBrand(page, limit, status_id, search_by){
        this.brandService.getBrandList(page, limit ,status_id ,search_by).then((response:any)=>{
            if(response['data'].length == 0){
                this.failAlert("No Result")
            }else{
                this.brand_list = response['data']
                this.total = response['total']
            }
        },error=>{
            this.failAlert(error.message)
        })
    }

    successAlert(message) {
        swal({
            type: 'success',
            title: 'Successful',
            text: message,
            confirmButtonClass: 'btn btn-success',
            buttonsStyling: false
        })
    }

    failAlert(message) {
        swal({
            title: 'Failed',
            text: message,
            type: 'error',
            confirmButtonClass: "btn btn-info",
            buttonsStyling: false
        })
    }

    loadingAlert() {
        swal({
            title: 'Loading...',
            text: ' ',
            onOpen: () => {
              swal.showLoading()
            }
        })
    }

    clearInput(){
        this.brand['name'] = "";
    }

    addModal() {
        $("#addModal").modal("show");
        $("#addModal").appendTo("body");
    }

    editModal(data) {
        this.brand['id'] = data['id']
        this.brand['name'] = data['name']

        $("#editModal").modal("show");
        $("#editModal").appendTo("body");
    }

    addBrand() {

        if((this.brand['name'] == "") || (this.brand['name'].length < 2)){
            this.failAlert("Name are required and minimum 2 characters.")
        }else {
            this.loadingAlert()
            this.brandService.addBrand(this.brand).then((response:any)=>{
                this.clearInput();
                $("#addModal").modal("hide");
                this.loadBrand(this.page, this.limit, this.status_id, this.search_name);
                this.successAlert(response.message)
            },error=>{
                this.failAlert(error.message)
            })
        }
    }

    editBrand(data) {
        if((this.brand['name'] == "") || (this.brand['name'].length < 2)){
            this.failAlert("Name are required and minimum 2 characters.")
        }else {
            this.loadingAlert()
            this.brandService.updateBrand(this.brand['id'], this.brand).then((response:any)=>{
               this.clearInput();
                $("#editModal").modal("hide");
                this.loadBrand(this.page, this.limit, this.status_id, this.search_name);
                this.successAlert(response.message)
            },error=>{
                this.failAlert(error.message)
            })
        }
    }

    deleteBrand(brand_id) {
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            confirmButtonText: 'Yes, delete it!',
            buttonsStyling: false
        }).then((confirm)=> {
            if(confirm) {
                this.brandService.deleteBrand(brand_id).then((response:any)=>{
                    this.loadBrand(this.page, this.limit, this.status_id, this.search_name);
                    this.successAlert(response.message)
                },error=>{
                    this.failAlert(error.message)
                })
            }
        }).catch(swal.noop)
    }

    goToPage(n: number): void {
        this.page = n;
        this.loadBrand(this.page, this.limit, this.status_id, this.search_name);
    }

    onNext(): void {
        this.page++;
        this.loadBrand(this.page, this.limit, this.status_id, this.search_name);
    }

    onPrev(): void {
        this.page--;
        this.loadBrand(this.page, this.limit, this.status_id, this.search_name);
    }
}
