import { Component, OnInit, AfterViewInit } from '@angular/core';
import { CategoryService } from '../providers/category-service';
import { AuthService } from '../providers/auth-service';

declare var $:any;
declare var swal:any;

@Component({
    moduleId: module.id,
    selector: 'category',
    templateUrl: 'category.component.html',
    styleUrls: ['category.component.scss']
})

export class CategoryComponent implements OnInit{

    status_id: any = "1";
    category_list: any;
    total: any;
    page: any = 1;
    limit: any = 10;
    search_name: any = "";
    category: any = {
        "name" : ""
    };

    constructor (public categoryService: CategoryService, public auth: AuthService){
    }

    ngOnInit(){
        if(this.auth.checkLogin()){
            this.loadCategory(this.page, this.limit, this.status_id, "");
        }
    }

    ngAfterViewInit(){
        // Init Tooltips
        $('[rel="tooltip"]').tooltip();
    }

    ngOnDestroy(){
        $("body>#addModal").remove();
        $("body>#editModal").remove();
    }

    onSearch(data){
        this.search_name = data
        this.page = 1
        this.loadCategory(this.page, this.limit, this.status_id, this.search_name)
    }

    loadCategory(page, limit, status_id, search_by){
        this.categoryService.getCategoryList(page, limit, status_id, search_by).then((response:any)=>{
            if(response['data'].length == 0){
                this.failAlert("No Result")
            }else{
                this.category_list = response['data']
                this.total = response['total']
            }
        },error=>{
            this.failAlert(error.message)
        })
    }

    successAlert(message) {
        swal({
            type: 'success',
            title: 'Successful',
            text: message,
            confirmButtonClass: 'btn btn-success',
            buttonsStyling: false
        })
    }

    failAlert(message) {
        swal({
            title: 'Failed',
            text: message,
            type: 'error',
            confirmButtonClass: "btn btn-info",
            buttonsStyling: false
        })
    }

    loadingAlert() {
        swal({
            title: 'Loading...',
            text: ' ',
            onOpen: () => {
              swal.showLoading()
            }
        })
    }

    clearInput(){
        this.category['name'] = "";
    }

    addModal() {
        $("#addModal").modal("show");
        $("#addModal").appendTo("body");
    }

    editModal(data) {
        this.category['id'] = data['id']
        this.category['name'] = data['name']

        $("#editModal").modal("show");
        $("#editModal").appendTo("body");
    }

    addCategory() {
        if((this.category['name'] == "") || (this.category['name'].length < 2)){
            this.failAlert("Name are required and minimum 2 characters.")
        }else {
            this.loadingAlert()
            this.categoryService.addCategory(this.category).then((response:any)=>{
                this.clearInput();
                $("#addModal").modal("hide");
                this.loadCategory(this.page, this.limit, this.status_id, this.search_name)
                this.successAlert(response.message)
            },error=>{
                this.failAlert(error.message)
            })
        }
    }

    editCategory() {
        if((this.category['name'] == "") || (this.category['name'].length < 2)){
            this.failAlert("Name are required and minimum 2 characters.")
        }else {
            this.loadingAlert()
            this.categoryService.updateCategory(this.category['id'], this.category).then((response:any)=>{
               this.clearInput();
                $("#editModal").modal("hide");
                this.loadCategory(this.page, this.limit, this.status_id, this.search_name)
                this.successAlert(response.message)
            },error=>{
                this.failAlert(error.message)
            })
        }
    }

    deleteCategory(category_id) {
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            confirmButtonText: 'Yes, delete it!',
            buttonsStyling: false
        }).then((confirm)=> {
            if(confirm) {
                this.categoryService.deleteCategory(category_id).then((response:any)=>{
                    this.loadCategory(this.page, this.limit, this.status_id, this.search_name)
                    this.successAlert(response.message)
                },error=>{
                    this.failAlert(error.message)
                })
            }
        }).catch(swal.noop)
    }

    goToPage(n: number): void {
        this.page = n;
        this.loadCategory(this.page, this.limit, this.status_id, this.search_name)
    }

    onNext(): void {
        this.page++;
        this.loadCategory(this.page, this.limit, this.status_id, this.search_name)
    }

    onPrev(): void {
        this.page--;
        this.loadCategory(this.page, this.limit, this.status_id, this.search_name)
    }
}
