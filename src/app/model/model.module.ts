import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { ModelComponent } from './model.component';
import { ModelRoutes } from './model.routing';
import { PaginationModule } from '../components/pagination/pagination.module';
import { KeysPipe } from '../shared/pipe/keys-pipe';
@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(ModelRoutes),
        FormsModule,
        PaginationModule,
        FormsModule 
    ],
    declarations: [ModelComponent]
})

export class ModelModule {}
