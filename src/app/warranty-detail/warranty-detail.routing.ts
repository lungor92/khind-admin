import { Routes } from '@angular/router';

import { WarrantyDetailComponent } from './warranty-detail.component';

export const WarrantyDetailRoutes: Routes = [{
        path: '',
        component: WarrantyDetailComponent
    }
];
