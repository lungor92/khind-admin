import { Component, OnInit, AfterViewInit } from '@angular/core';
import { WarrantyService } from '../providers/warranty-service';
import { Router } from '@angular/router';
import { Data } from '../providers/data';
import { AuthService } from '../providers/auth-service';

declare var $:any;
declare var swal:any;

@Component({
    moduleId: module.id,
    selector: 'warranty',
    templateUrl: 'warranty.component.html',
    styleUrls: ['warranty.component.scss']
})

export class WarrantyComponent implements OnInit{

    warranty_list: any;
    total: any;
    page: any = 1;
    limit: any = 10;
    search_name: any = "";

    constructor (public warrantyService: WarrantyService, public router: Router, public data: Data, public auth: AuthService){
    }

    ngOnInit(){
        if(this.auth.checkLogin()){
            this.loadWarranty(this.page, this.limit, "");
        }
    }

    ngAfterViewInit(){
        // Init Tooltips
        $('[rel="tooltip"]').tooltip();
    }

    onSearch(data){
        this.search_name = data
        this.page = 1
        this.loadWarranty(1, this.limit, this.search_name)
    }

    goToEdit(data){
        this.data.storage = data;
        this.router.navigate(['../warranty-detail'])
    }

    loadWarranty(page, limit, search_by){
        this.warrantyService.getWarrantyList(page, limit, search_by).then((response:any)=>{
            if(response['data'].length == 0){
                this.failAlert("No Result")
            }else{
                this.warranty_list = response['data']
                this.total = response['total']
            }
        },error=>{
            this.failAlert(error.message)
        })
    }

    successAlert(message) {
        swal({
            type: 'success',
            title: 'Successful',
            text: message,
            confirmButtonClass: 'btn btn-success',
            buttonsStyling: false
        })
    }

    failAlert(message) {
        swal({
            title: 'Failed',
            text: message,
            type: 'error',
            confirmButtonClass: "btn btn-info",
            buttonsStyling: false
        })
    }

    loadingAlert() {
        swal({
            title: 'Loading...',
            text: ' ',
            onOpen: () => {
              swal.showLoading()
            }
        })
    }

    goToPage(n: number): void {
        this.page = n;
        this.loadWarranty(this.page, this.limit, this.search_name);
    }

    onNext(): void {
        this.page++;
        this.loadWarranty(this.page, this.limit, this.search_name);
    }

    onPrev(): void {
        this.page--;
        this.loadWarranty(this.page, this.limit, this.search_name);
    }
}
