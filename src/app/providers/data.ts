import { Injectable } from '@angular/core';

@Injectable()
export class Data {

    public storage: any;

    public constructor() { }

    public storeData(data) {
        this.storage = data;
    }

    public getData() {
        return this.storage;
    }
}
