import { Routes } from '@angular/router';

import { AdminLayoutComponent } from './layouts/admin/admin-layout.component';
import { AuthLayoutComponent } from './layouts/auth/auth-layout.component';

export const AppRoutes: Routes = [{
        path: '',
        redirectTo: '/pages/login',
        pathMatch: 'full',
    }, {
        path: '',
        component: AdminLayoutComponent,
        children: [{
            path: 'dashboard',
            loadChildren: './dashboard/dashboard.module#DashboardModule'
        },{
            path: 'components',
            loadChildren: './components/components.module#ComponentsModule'
        },{
            path: 'forms',
            loadChildren: './forms/forms.module#Forms'
        },{
            path: 'tables',
            loadChildren: './tables/tables.module#TablesModule'
        },{
            path: '',
            loadChildren: './userpage/user.module#UserModule'
        },{
            path: '',
            loadChildren: './timeline/timeline.module#TimelineModule'
        },{
            path: 'brand',
            loadChildren: './brand/brand.module#BrandModule'
        },{
            path: 'category',
            loadChildren: './category/category.module#CategoryModule'
        },{
            path: 'product-type',
            loadChildren: './product-type/product-type.module#ProductTypeModule'
        },{
            path: 'model',
            loadChildren: './model/model.module#ModelModule'
        },{
            path: 'warranty',
            loadChildren: './warranty/warranty.module#WarrantyModule'
        },{
            path: 'warranty-detail',
            loadChildren: './warranty-detail/warranty-detail.module#WarrantyDetailModule'
        }]
        },{
            path: '',
            component: AuthLayoutComponent,
            children: [{
                path: 'pages',
                loadChildren: './pages/pages.module#PagesModule'
            }]
        }
];
