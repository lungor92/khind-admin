import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ProductTypeService } from '../providers/product-type-service';
import { CategoryService } from '../providers/category-service';
import { AuthService } from '../providers/auth-service';

declare var $:any;
declare var swal:any;

@Component({
    moduleId: module.id,
    selector: 'product-type',
    templateUrl: 'product-type.component.html',
    styleUrls: ['product-type.component.scss']
})

export class ProductTypeComponent implements OnInit{
    status_id: any = "1";
    product_type_list: any;
    category_list: any;
    total: any;
    page: any = 1;
    limit: any = 10;
    search_name: any = "";
    product:any = {
        "name": "",
        "category_id" : "",
    };
    
    constructor (public productTypeService: ProductTypeService, public categoryService:CategoryService, public auth:AuthService){
    }

    ngOnInit(){
        if(this.auth.checkLogin()){
            this.loadProductType(this.page, this.limit, this.status_id, "");
            this.loadCategory();
        }
    }

    ngAfterViewInit(){
        // Init Tooltips
        $('[rel="tooltip"]').tooltip();
    }

    ngOnDestroy(){
        $("body>#addModal").remove();
        $("body>#editModal").remove();
    }

    onSearch(data){
        this.search_name = data
        this.page = 1
        this.loadProductType(this.page, this.limit, this.status_id, this.search_name)
    }

    loadCategory(){
        this.categoryService.getCategoryList(1,100,1,"").then((response:any)=>{
            this.category_list = response['data']
        },error=>{
        })
    }

    loadProductType(page, limit, status_id, search_by){
        this.productTypeService.getProductTypeList(page, limit, status_id, search_by).then((response:any)=>{
            if(response['data'].length == 0){
                this.failAlert("No Result")
            }else{
                this.product_type_list = response['data']
                this.total = response['total']
            }
        },error=>{
            this.failAlert(error.message)
        })
    }

    successAlert(message) {
        swal({
            type: 'success',
            title: 'Successful',
            text: message,
            confirmButtonClass: 'btn btn-success',
            buttonsStyling: false
        })
    }

    failAlert(message) {
        swal({
            title: 'Failed',
            text: message,
            type: 'error',
            confirmButtonClass: "btn btn-info",
            buttonsStyling: false
        })
    }

    loadingAlert() {
        swal({
            title: 'Loading...',
            text: ' ',
            onOpen: () => {
              swal.showLoading()
            }
        })
    }

    clearInput(){
        this.product = {
            "name": "",
            "category_id" : ""
        }
    }

    addModal() {
        this.clearInput();
        $("#addModal").modal("show");
        $("#addModal").appendTo("body");
    }

    editModal(data) {
        this.product = data
        $("#editModal").modal("show");
        $("#editModal").appendTo("body");
    }

    addProductType() {

        if((this.product['name'] == "") || (this.product['name'].length < 2)){
            this.failAlert("Name are required and minimum 2 characters.")
        }else if(this.product['category_id'] == null){
            this.failAlert("Please choose one category.")
        }else {
            this.loadingAlert()
            this.productTypeService.addProductType(this.product).then((response:any)=>{
                $("#addModal").modal("hide");
                this.loadProductType(this.page, this.limit, this.status_id, this.search_name);
                this.successAlert(response.message)
            },error=>{
                this.failAlert(error.message)
            })
        }
    }

     editProductType() {

        if((this.product['name'] == "") || (this.product['name'].length < 2)){
            this.failAlert("Name are required and minimum 2 characters.")
        }else if(this.product['category_id'] == null){
            this.failAlert("Please choose one category.")
        }else {
            this.loadingAlert()
            this.productTypeService.updateProductType(this.product['id'], this.product).then((response:any)=>{
                $("#editModal").modal("hide");
                this.loadProductType(this.page, this.limit, this.status_id, this.search_name);
                this.successAlert(response.message)
            },error=>{
                this.failAlert(error.message)
            })
        }
    }

    deleteProductType(product_type_id) {
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            confirmButtonText: 'Yes, delete it!',
            buttonsStyling: false
        }).then((confirm)=> {
            if(confirm) {
                this.productTypeService.deleteProductType(product_type_id).then((response:any)=>{
                    this.loadProductType(this.page, this.limit, this.status_id, this.search_name);
                    this.successAlert(response.message)
                },error=>{
                    this.failAlert(error.message)
                })
            }
        }).catch(swal.noop)
    }

    goToPage(n: number): void {
        this.page = n;
        this.loadProductType(this.page, this.limit, this.status_id, this.search_name);
    }

    onNext(): void {
        this.page++;
        this.loadProductType(this.page, this.limit, this.status_id, this.search_name);
    }

    onPrev(): void {
        this.page--;
        this.loadProductType(this.page, this.limit, this.status_id, this.search_name);
    }
}
