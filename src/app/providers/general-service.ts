import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { RouterModule, Router, ActivatedRoute } from '@angular/router';
import { API_URL, grant_type, client_id, client_secret, scope } from '../config/constants';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import { reject, resolve } from 'q';
import { AuthService } from './auth-service';

@Injectable()
export class GeneralService {
  constructor(private http: Http, public router: Router, public authService: AuthService) { }

  getLocation(postcode): Promise<any> {
    let headers = new Headers();
    this.authService.createAuthorizationHeader(headers);
    this.authService.createJsonHeader(headers);

    return this.http.get(API_URL + 'api/city/'+postcode, {
      headers: headers
    }).toPromise().then(response => response.json())
  }
}
