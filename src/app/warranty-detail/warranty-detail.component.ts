import { Component, OnInit, AfterViewInit } from '@angular/core';
import { WarrantyService } from '../providers/warranty-service';
import { Data } from '../providers/data';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { BrandService } from '../providers/brand-service';
import { ProductTypeService } from '../providers/product-type-service';
import { CategoryService } from '../providers/category-service';
import { ModelService } from '../providers/model-service';
import { GeneralService } from '../providers/general-service';
import { AuthService } from '../providers/auth-service';

declare var $:any;
declare var swal:any;

@Component({
    moduleId: module.id,
    selector: 'warranty-detail',
    templateUrl: 'warranty-detail.component.html',
    styleUrls: ['warranty-detail.component.scss']
})

export class WarrantyDetailComponent implements OnInit{

    warranty_info: any;
    warranty_form: FormGroup;
    total: any;
    page: any = 1;
    limit: any = 10;
    search_name: any;
    brand_list: any;
    product_type_list: any;
    category_list: any;
    model_list: any;
    city_list:any;
    state:any;
    country:any;
    city_id: any;

    constructor (
        public warrantyService: WarrantyService,
        public brandService: BrandService,
        public productTypeService: ProductTypeService,
        public categoryService: CategoryService,
        public modelServie: ModelService,
        public data: Data,
        public router: Router,
        public generalService: GeneralService,
        public auth: AuthService
    ){
        this.warranty_form = new FormGroup({
            date_purchase: new FormControl('', Validators.required),
            serial_no: new FormControl('', Validators.required),
            first_name: new FormControl('', Validators.required),
            last_name: new FormControl('', Validators.required),
            telephone_no: new FormControl('', Validators.required),
            address_1: new FormControl('', Validators.required),
            address_2: new FormControl('', Validators.required),
            address_3: new FormControl('', Validators.required),
            city_id: new FormControl('', Validators.required),
            state_id: new FormControl('', Validators.required),
            country_id: new FormControl('', Validators.required),
            postcode: new FormControl('', Validators.required),
            email: new FormControl('', Validators.required),
            model_no: new FormControl('', Validators.required),
            model_id: new FormControl('', Validators.required)
        })
    }

    ngOnInit(){
        if(this.auth.checkLogin()){
            this.loadModel();
            this.warranty_info = this.data.getData();

            if(this.warranty_info == undefined){
                this.router.navigate(['../warranty'])
            }else{
                this.city_id = this.warranty_info['city_id'];
                this.getLocation(this.warranty_info['city']['postcode'])
            }
        }
        
    }

    successAlert(message) {
        swal({
            type: 'success',
            title: 'Successful',
            text: message,
            confirmButtonClass: 'btn btn-success',
            buttonsStyling: false
        })
    }

    failAlert(message) {
        swal({
            title: 'Failed',
            html: message,
            type: 'error',
            confirmButtonClass: "btn btn-info",
            buttonsStyling: false
        })
    }

    loadingAlert() {
        swal({
            title: 'Loading...',
            text: ' ',
            onOpen: () => {
              swal.showLoading()
            }
        })
    }

    getLocation(postcode){
        if(this.warranty_info != undefined){
            this.generalService.getLocation(postcode).then(data=>{
                if(data.length > 0){
                    this.city_list = data;
                    this.city_id = data[0]['id']
                    this.warranty_info['state'] = data[0]['state'];
                    this.warranty_info['country'] = data[0]['state']['country'];
                }else{
                    this.failAlert("Postal Code not found") 
                }
            })
        }
    }

    loadModel(){
        this.modelServie.getModelList(1,100,1,"").then(data=>{
            this.model_list = data['data']
        })
    }

    doSubmit(data){
        this.loadingAlert()
        data['telephone_country'] = "MY";
        this.warrantyService.updateWarranty(this.warranty_info['id'], data).then((response:any)=>{
            this.successAlert(response.message)
        },error=>{console.log(error)

            let newError = $.map(error, function (value, index) {
                return [value];
            });

            let errorMessage:any = '';
            newError.forEach(function (value) {
                errorMessage += '<br><span>- '+ value + '</span>'
            })

            this.failAlert(errorMessage)
        })
    }
}
