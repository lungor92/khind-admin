import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { RouterModule, Router, ActivatedRoute } from '@angular/router';
import { API_URL, grant_type, client_id, client_secret, scope } from '../config/constants';
import 'rxjs/add/operator/map';
import { reject } from 'q';

@Injectable()
export class AuthService {
  constructor(private http: Http, public router: Router) { }

  login(data) {
    return new Promise((resolve,reject) => {
      data.grant_type = grant_type;
      data.client_id = client_id;
      data.client_secret = client_secret;
      data.scope = scope;
      this.http.post(API_URL + 'oauth/token', data)
        .map(res => res.json())
        .subscribe(
        data => {
          if (data.token_type === 'Bearer') {
            localStorage.setItem('token', data.access_token);
            resolve(data);
          }
        },
        error => {
          reject(error.json());
        });
    });
  }

  logout() {
    localStorage.removeItem('token');
    this.router.navigate(['/pages/login'])
  }

  public localStorageItem(item): string {
    return localStorage.getItem(item);
  }

  public checkLogin() {
    if(this.localStorageItem('token')) {
      return true;
    }else {
      this.router.navigate(['/pages/login'])
    }
  }

  createJsonHeader(headers: Headers) {
    headers.append('Accept', 'application/json');
  }

  createAuthorizationHeader(headers: Headers) {
    headers.append('Authorization', 'Bearer ' + this.localStorageItem('token'));
  }
}
