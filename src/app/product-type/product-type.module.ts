import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { ProductTypeComponent } from './product-type.component';
import { ProductTypeRoutes } from './product-type.routing';
import { PaginationModule } from '../components/pagination/pagination.module';
import { KeysPipe } from '../shared/pipe/keys-pipe';
@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(ProductTypeRoutes),
        FormsModule,
        PaginationModule,
        FormsModule 
    ],
    declarations: [ProductTypeComponent, KeysPipe]
})

export class ProductTypeModule {}
