import { Routes } from '@angular/router';

import { WarrantyComponent } from './warranty.component';

export const WarrantyRoutes: Routes = [{
        path: '',
        component: WarrantyComponent
    }
];
