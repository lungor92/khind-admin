import { Component, OnInit, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location, LocationStrategy, PathLocationStrategy } from '@angular/common';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { AuthService } from '../../providers/auth-service';

declare var $:any;
declare var swal:any;

@Component({
    moduleId:module.id,
    selector: 'login-cmp',
    templateUrl: './login.component.html'
})

export class LoginComponent implements OnInit{
    test : Date = new Date();
    private toggleButton;
    private sidebarVisible: boolean;
    private nativeElement: Node;

    login_form: FormGroup;

    constructor(private element : ElementRef, public authService: AuthService, public router: Router) {
        this.nativeElement = element.nativeElement;
        this.sidebarVisible = false;

        this.login_form = new FormGroup({
            username: new FormControl('', Validators.required),
            password: new FormControl('', Validators.required)
        })
    }
    checkFullPageBackgroundImage(){
        var $page = $('.full-page');
        var image_src = $page.data('image');

        if(image_src !== undefined){
            var image_container = '<div class="full-page-background" style="background-image: url(' + image_src + ') "/>'
            $page.append(image_container);
        }
    };

    ngOnInit(){
        this.checkFullPageBackgroundImage();

        var navbar : HTMLElement = this.element.nativeElement;
        this.toggleButton = navbar.getElementsByClassName('navbar-toggle')[0];

        setTimeout(function(){
            // after 1000 ms we add the class animated to the login/register card
            $('.card').removeClass('card-hidden');
        }, 700)

        if(this.authService.checkLogin()){
            this.router.navigate(['/brand']);
        }
    }

    doLogin(data){
        swal({
            title: 'Logging in',
            onOpen: () => {
              swal.showLoading()
            }
        })
          
        this.authService.login(data).then((response:any)=>{
            swal.close();
            this.router.navigate(['/brand']);
        },error=>{
            this.showModal("Failed" ,error.message)
        })
    }

    showModal(title, message){
        swal({
            title: title,
            text: message,
            type: 'error',
            confirmButtonText: 'Try again',
            confirmButtonClass: "btn btn-info",
            buttonsStyling: false
        })
    }

    sidebarToggle(){
        var toggleButton = this.toggleButton;
        var body = document.getElementsByTagName('body')[0];
        var sidebar = document.getElementsByClassName('navbar-collapse')[0];
        if(this.sidebarVisible == false){
            setTimeout(function(){
                toggleButton.classList.add('toggled');
            },500);
            body.classList.add('nav-open');
            this.sidebarVisible = true;
        } else {
            this.toggleButton.classList.remove('toggled');
            this.sidebarVisible = false;
            body.classList.remove('nav-open');
        }
    }
}
