import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';
import { APP_BASE_HREF } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';

import { SidebarModule } from './sidebar/sidebar.module';
import { FixedPluginModule } from './shared/fixedplugin/fixedplugin.module';
import { FooterModule } from './shared/footer/footer.module';
import { NavbarModule} from './shared/navbar/navbar.module';
import { AdminLayoutComponent } from './layouts/admin/admin-layout.component';
import { AuthLayoutComponent } from './layouts/auth/auth-layout.component';
import { AppRoutes } from './app.routing';

import { AuthService } from './providers/auth-service';
import { BrandService } from './providers/brand-service';
import { CategoryService } from './providers/category-service';
import { ProductTypeService } from './providers/product-type-service';
import { ModelService } from './providers/model-service';
import { WarrantyService } from './providers/warranty-service';
import { Data } from './providers/data';
import { GeneralService } from './providers/general-service';

const APP_PROVIDERS = [ AuthService, BrandService, CategoryService, ProductTypeService, ModelService, WarrantyService, Data, GeneralService ];

@NgModule({
    imports:      [
        BrowserModule,
        FormsModule,
        RouterModule.forRoot(AppRoutes),
        HttpModule,
        SidebarModule,
        NavbarModule,
        FooterModule,
        FixedPluginModule
    ],
    declarations: [
        AppComponent,
        AdminLayoutComponent,
        AuthLayoutComponent,
    ],
    bootstrap:    [ AppComponent ],
    providers : [ APP_PROVIDERS ]
})

export class AppModule { }
