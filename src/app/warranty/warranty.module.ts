import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { WarrantyComponent } from './warranty.component';
import { WarrantyRoutes } from './warranty.routing';
import { PaginationModule } from '../components/pagination/pagination.module';
@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(WarrantyRoutes),
        FormsModule,
        PaginationModule
    ],
    declarations: [WarrantyComponent]
})

export class WarrantyModule {}
