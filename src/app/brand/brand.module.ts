import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { BrandComponent } from './brand.component';
import { BrandRoutes } from './brand.routing';
import { PaginationModule } from '../components/pagination/pagination.module';
@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(BrandRoutes),
        FormsModule,
        PaginationModule
    ],
    declarations: [BrandComponent]
})

export class BrandModule {}
