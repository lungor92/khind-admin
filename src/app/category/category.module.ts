import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { CategoryComponent } from './category.component';
import { CategoryRoutes } from './category.routing';
import { PaginationModule } from '../components/pagination/pagination.module';
@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(CategoryRoutes),
        FormsModule,
        PaginationModule
    ],
    declarations: [CategoryComponent]
})

export class CategoryModule {}
