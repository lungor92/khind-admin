import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { RouterModule, Router, ActivatedRoute } from '@angular/router';
import { API_URL, grant_type, client_id, client_secret, scope } from '../config/constants';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import { reject, resolve } from 'q';
import { AuthService } from './auth-service';

@Injectable()
export class WarrantyService {
  constructor(private http: Http, public router: Router, public authService: AuthService) { }

  getWarrantyList(page, limit, search_by): Promise<any> {
    let headers = new Headers();
    this.authService.createAuthorizationHeader(headers);
    this.authService.createJsonHeader(headers);

    return this.http.get(API_URL + 'api/warranty?page='+page+'&per_page'+limit+'&search='+search_by, {
      headers: headers
    }).toPromise().then(response => response.json())
  }

  searchWarranty(data): Promise<any> {
    let headers = new Headers();
    this.authService.createAuthorizationHeader(headers);
    this.authService.createJsonHeader(headers);

    return this.http.get(API_URL + 'api/warranty?search='+data+'&page=1&per_page=10', {
      headers: headers
    }).toPromise().then(response => response.json())
  }

  updateWarranty(warranty_id, data) {
    let headers = new Headers();
    this.authService.createAuthorizationHeader(headers);
    this.authService.createJsonHeader(headers);
    
    return new Promise((resolve,reject)=>{
      this.http.put(API_URL + 'api/warranty/' + warranty_id, data, {
        headers: headers
      }).map(res => res.json())
      .subscribe(data=>{
        resolve(data)
      },error=>{
        reject(error.json())
      })
    })
  }
}
