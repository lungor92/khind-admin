import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'flatten'
})
export class KeysPipe implements PipeTransform {
  transform(arr: any[], property: string) {
    if(!arr) return [];
    return arr.reduce((acc, cur) => {
      return [...acc, cur, ...this.transform(cur[property], property)];
    }, [])
  }
}