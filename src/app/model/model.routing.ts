import { Routes } from '@angular/router';

import {ModelComponent } from './model.component';

export const ModelRoutes: Routes = [{
        path: '',
        component: ModelComponent
    }
];
