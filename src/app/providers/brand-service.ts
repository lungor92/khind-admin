import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { RouterModule, Router, ActivatedRoute } from '@angular/router';
import { API_URL, grant_type, client_id, client_secret, scope } from '../config/constants';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import { reject, resolve } from 'q';
import { AuthService } from './auth-service';

@Injectable()
export class BrandService {
  constructor(private http: Http, public router: Router, public authService: AuthService) { }

  getBrandList(page, limit, status_id, search_by): Promise<any> {
    let headers = new Headers();
    this.authService.createAuthorizationHeader(headers);
    this.authService.createJsonHeader(headers);

    return this.http.get(API_URL + 'api/brand?page='+page+'&per_page'+limit+'&status='+ status_id+'&search='+search_by, {
      headers: headers
    }).toPromise().then(response => response.json())
  }

  searchBrand(data): Promise<any> {
    let headers = new Headers();
    this.authService.createAuthorizationHeader(headers);
    this.authService.createJsonHeader(headers);

    return this.http.get(API_URL + 'api/brand?search='+data+'&page=1&per_page=10&status=1', {
      headers: headers
    }).toPromise().then(response => response.json())
  }

  addBrand(data) {
    let headers = new Headers();
    this.authService.createAuthorizationHeader(headers);
    this.authService.createJsonHeader(headers);
    console.log(data)
    return new Promise((resolve,reject)=>{
      this.http.post(API_URL + 'api/brand', data, {
        headers: headers
      }).map(res => res.json())
      .subscribe(data=>{console.log(data)
        resolve(data)
      },error=>{console.log(error)
        reject(error.json())
      })
    })
  }

  updateBrand(brand_id, data) {
    let headers = new Headers();
    this.authService.createAuthorizationHeader(headers);
    this.authService.createJsonHeader(headers);
    console.log(data)
    return new Promise((resolve,reject)=>{
      this.http.put(API_URL + 'api/brand/' + brand_id, data, {
        headers: headers
      }).map(res => res.json())
      .subscribe(data=>{console.log(data)
        resolve(data)
      },error=>{console.log(error)
        reject(error.json())
      })
    })
  }

  deleteBrand(brand_id) {
    let headers = new Headers();
    this.authService.createAuthorizationHeader(headers);
    this.authService.createJsonHeader(headers);
    
    return new Promise((resolve,reject)=>{
      this.http.delete(API_URL + 'api/brand/' + brand_id, {
        headers: headers
      }).map(res => res.json())
      .subscribe(data=>{
        resolve(data)
      },error=>{console.log(error)
        reject(error.json())
      })
    })
  }
}
